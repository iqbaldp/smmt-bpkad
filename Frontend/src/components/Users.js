import { Scrollbars } from 'react-custom-scrollbars';
import React, { Component } from 'react';
import {
  Route,
  Switch
} from "react-router-dom";
import axios from 'axios'
import '../animate.css';
import {Animated} from "react-animated-css";
import cookie from 'react-cookies';

class Users extends Component {

    constructor(props){
        super(props)
        this.state = {
            user    : [],
            nip     :'',
            id      : '',
            nama    : '',
            nipH     :'',
            idH      : '',
            namaH    : '',
            jabatan : '',
            password: '',
            success : false,
            length : 0,
            show : false
        }
        this.handleDeleteUser = this.handleDeleteUser.bind(this)
    }

    handleCloseModal = () => {
        this.setState({ show : false})
    }

    handleGetData = (id, nip, nama) => {
        const self = this
        self.setState({
            idH : id,
            namaH : nama,
            nipH : nip
        })
    }

    handleNIP = (e) => {
        this.setState({nip : e.target.value})
    }
    handleNama = (e) => {
        this.setState({nama: e.target.value})
    }
    handleJabatan = (e) => {
        this.setState({jabatan : e.target.value})
    }
    handlePassword = (e) => {
        this.setState({password : e.target.value})
    }

    componentDidMount(){
        const self = this
        axios.get('http://localhost:8000/api/users')
        .then(function (response) {
            let users = Object.keys(response.data).length
            self.setState({user : response.data, length : users})
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    handleDeleteUser(){
        const self = this
        axios.delete('http://localhost:8000/api/users/' + self.state.idH)
        .then(function (response) {
            let users = Object.keys(response.data).length
            self.setState({user : response.data, length : users})
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    handleUpdateUser(id, nip,  nama, jabatan){
        const self = this
        this.setState({
            nip, id, nama, jabatan, show : true, success : false
        })
    }

    handleSubmitUser = () => {
        const self = this
        if(self.state.jabatan == ''){
            console.log("kosong boyyy")
        }

        axios.put('http://localhost:8000/api/users/' + this.state.id, {
           nip      : this.state.nip,
           nama     : this.state.nama,
           jabatan  : this.state.jabatan,
           password : this.state.password
        })
        .then(function (response) {
            console.log(response.data)
            self.setState({
                user    : response.data[0],
                success : true,
                show    : false,
                password : ''
            })
        })
        .catch(function (error) {
            console.log(error);
        });
    }

  render() {
    return (

        <main class="main-wrapper clearfix">

            <div class="row page-title clearfix">
                <div class="page-title-left">
                    <h6 class="page-title-heading mr-0 mr-r-5">Kelola Pengguna</h6>
                    <p class="page-title-description mr-0 d-none d-md-inline-block"> [ Total : {this.state.length} Pengguna ]</p>
                </div>
            </div>
            
            <br/>

            {
                this.state.success ?
                <Animated animationIn="rubberBand" animationOut="pulse" isVisible={this.state.success}>
                <div class="alert alert-icon alert-success border-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button> <i class="material-icons list-icon">check_circle</i>  <strong>Sukses!</strong> Informasi pegawai berhasil diubah.</div>
                </Animated>
                : null
            }

            <div class="widget-list row">
                <div class="widget-holder widget-full-height col-md-12">
                    <div class="widget-bg">

                        <div class="widget-body">
                            
                        {
                            this.state.show ?
                            <Animated animationIn="bounceInUp" animationOut="pulse" isVisible={this.state.show}>
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <input onChange={this.handleNIP} class="form-control" value={this.state.nip} type="text" />
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <input onChange={this.handleNama} class="form-control" value={this.state.nama} type="text" />
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <div class="form-group">
                                    <select class="form-control" id="l13" onChange={this.handleJabatan}>
                                        <option>Pilih Jabatan</option>   
                                        <option value="Kepala BPKAD">Kepala BPKAD</option>      
                                        <option value="Sekretaris">Sekretaris</option>  
                                        <option value="Kasubbag Program">Kasubbag Program</option>
                                        <option value="Kasubbag Umum">Kasubbag Umum</option>
                                        <option value="Kasubbag Keuangan">Kasubbag Keuangan</option>    
                                        <option value="Kepala Bidang Anggaran">Kepala Bidang Anggaran</option>  
                                        <option value="Kasubbid Anggaran I">Kasubbid Anggaran I</option>
                                        <option value="Kasubbid Anggaran II">Kasubbid Anggaran II</option>
                                        <option value="Kasubbid Bina dan Evaluasi Anggaran Kabupaten/Kota">Kasubbid Bina dan Evaluasi Anggaran Kabupaten/Kota</option>
                                        <option value="Kepala Bidang Akuntansi dan Pelaporan">Kepala Bidang Akuntansi dan Pelaporan</option>     
                                        <option value="Kasubbid Akuntansi I">Kasubbid Akuntansi I</option>
                                        <option value="Kasubbid Akuntansi II">Kasubbid Akuntansi II</option>
                                        <option value="Kasubbid Pelaporan dan Evaluasi Keuangan Daerah">Kasubbid Pelaporan dan Evaluasi Keuangan Daerah</option>
                                        <option value="Kepala Bidang Perbendaharaan">Kepala Bidang Perbendaharaan</option>
                                        <option value="Kasubbid Pengelolaan KAS">Kasubbid Pengelolaan KAS</option>
                                        <option value="Kasubbid Perbendaharaan I">Kasubbid Perbendaharaan I</option>
                                        <option value="Kasubbid Perbendaharaan II">Kasubbid Perbendaharaan II</option>
                                        <option value="Kepala Bidang Pengelolaan BMD">Kepala Bidang Pengelolaan BMD</option>
                                        <option value="Kasubbid Perencanaan Kebutuhan dan Pengadaan BMD">Kasubbid Perencanaan Kebutuhan dan Pengadaan BMD</option>
                                        <option value="Kasubbid Pemeliharaan dan Penghapusan BMD">Kasubbid Pemeliharaan dan Penghapusan BMD</option> 
                                        <option value="Kasubbid Penatausahaan dan Pembinaan Aset">Kasubbid Penatausahaan dan Pembinaan Aset</option>
                                        <option value="Kepala Unit Pengelola Islamic Center">Kepala Unit Pengelola Islamic Center</option>
                                        <option value="Kasubbag Tata Usaha Pengelola Islamic Center">Kasubbag Tata Usaha Pengelola Islamic Center</option>
                                        <option value="Kasi Pemeliharaan Sarana dan Prasarana">Kasi Pemeliharaan Sarana dan Prasarana</option>
                                        <option value="Kasi Pemanfaatan, Pengembangan Usaha dan Bisnis">Kasi Pemanfaatan, Pengembangan Usaha dan Bisnis</option>
                                        <option value="Kepala UPTD Balai Pemanfaatan dan Pengamanan Aset">Kepala UPTD Balai Pemanfaatan dan Pengamanan Aset</option>
                                        <option value="Kasubbag Tata Usaha UPTD">Kasubbag Tata Usaha UPTD</option>  
                                        <option value="Kasi Pemanfaatan Aset">Kasi Pemanfaatan Aset</option>
                                        <option value="Kasi Pengamanan Aset">Kasi Pengamanan Aset</option>
                                        <option value="Staf">Staf</option>
                                    </select>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input onChange={this.handlePassword} value={this.state.password} class="form-control" placeholder="Kata sandi baru" type="password" />
                                        <p class="text-primary">*Diisi jika diperlukan</p>
                                    </div>
                                </div>
                                <div class="form-actions btn-list">
                                    <button onClick={this.handleSubmitUser} class="btn btn-primary" type="button">Ubah</button>
                                    <button onClick={this.handleCloseModal} class="btn btn-default" type="button">Tutup</button>
                                </div>

                            </div> 
                            </Animated>
                            : null
                        }

                        <div class="widget-list">
                            <div class="row">
                                <div class="col-md-12 widget-holder">
                                    <div class="widget-bg">
                                        <div class="widget-body clearfix">
                                            <Scrollbars style={{ width: '100%', height: 400 }}>
                                                <table class="table table-editable table-responsive">
                                                    <thead>
                                                        <tr>
                                                            <th>NIP</th>
                                                            <th data-editable="">Nama</th>
                                                            <th>Jabatan</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {
                                                            this.state.user.map((pengguna) => {
                                                                return(
                                                                    <tr>
                                                                        <td>{pengguna.nip}</td>
                                                                        <td>{pengguna.nama}</td>
                                                                        <td>{pengguna.jabatan}</td>
                                                                        <td><b onClick={() =>  this.handleUpdateUser(pengguna.id, pengguna.nip, pengguna.nama, pengguna.jabatan)} style={{'cursor' : 'pointer'}} class="text-warning">Sunting</b></td>
                                                                        <td><b onClick={() =>  this.handleGetData(pengguna.id, pengguna.nip, pengguna.nama)} style={{'cursor' : 'pointer'}} data-toggle="modal" data-target=".bs-modal-md" class="text-danger">Hapus</b></td>
                                                                    </tr>
                                                                )
                                                            })
                                                        }
                                                    </tbody>

                                                </table>
                                            </Scrollbars>

                                            <div class="modal modal-danger fade bs-modal-md" tabindex="-1" role="dialog" aria-labelledby="myMediumModalLabel" aria-hidden="true" style={{"display": "none"}}>
                                            <div class="modal-dialog modal-md">
                                                <div class="modal-content">
                                                    <div class="modal-header text-inverse">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                        <h5 class="modal-title" id="myMediumModalLabel">Hapus Pengguna</h5>
                                                    </div>
                                                    <div class="modal-body">
                                                        <h5>Apakah anda yakin ingin menghapus pegawai ini ?</h5>
                                                        <p> NIP  : {this.state.nipH}</p>
                                                        <p> Nama : {this.state.namaH}</p>
                                                    </div>
                                                    <div class="modal-footer"><a href="#" data-dismiss="modal" class="btn btn-info btn-rounded ripple text-left">Batal</a> 
                                                        <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal" onClick={this.handleDeleteUser}>Hapus</button>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            </div>
                                           
                                            
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                                
                            </div>
                            
                        </div>
                            
                        </div>
                        
                    </div>
                    
                </div>
                
                
                
            </div>
            
      </main>

    );
  }
}

export default Users;
