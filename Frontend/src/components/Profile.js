import React, { Component } from 'react';
import {
  Route,
  Switch
} from "react-router-dom";
import axios from 'axios'
import { BounceLoader, ClipLoader } from 'react-spinners'
import Loading from 'react-loading-bar'
import 'react-loading-bar/dist/index.css'
import cookie from 'react-cookies';

class Detail extends Component {

    constructor(props){
        super(props)

        this.state = {
            person      : [],
            task        : [],
            loading     : true,
            isi         : true,
            selectedFile : null,
            loadImage   : false
        }

    }

    fileSelectedHandler = event => {
        this.setState({
          selectedFile : event.target.files[0],
          loadImage : true
        }, (event) => {

            this.fileUploadHandler();

        })
    }

    fileUploadHandler = (event) => {
        const self = this;

        const fd = new FormData()
        fd.append('image', this.state.selectedFile)
        axios.defaults.headers.common['Authorization'] = 'Client-ID af3942dc3f3cd90'

        axios.post('http://localhost:8000/api/auth/signupimage', fd)
        .then(function (response) {
            console.log(response.data.name)
            self.registerUser(response.data.name)
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    registerUser = (foto) => {
        const self = this

        let person = Object.assign({}, this.state.person);   
        person.foto = foto                       
        this.setState({
            person
        });

        axios.put('http://localhost:8000/api/users/image/' + cookie.load('user_id'), {
            foto : foto
          })
          .then(function (response) {
            self.setState({
                loadImage : false
            })
          })
          .catch(function (error) {
            console.log(error);
        });

    }

    componentDidMount(){
        const self = this
        //load profil
        if(this.props.match.params.name == undefined){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + cookie.load('token')
            axios.get('http://localhost:8000/api/profile')
            .then(function (response) {
                self.setState({
                    person    : response.data,
                    loading : false
                })
            })
            .catch(function (error) {
                console.log(error);
            });
        }else{
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + cookie.load('token')
            axios.get('http://localhost:8000/api/users/' + this.props.match.params.name)
            .then(function (response) {
                self.setState({
                    person    : response.data[0],
                    loading : false
                })
            })
            .catch(function (error) {
                console.log(error);
            });
        }
    }

    handleRiwayat = () => {
        const self = this
        self.setState({
            loading : true
        })

        //load task
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + cookie.load('token')
        axios.get('http://localhost:8000/api/taskreceive/' + this.state.person.id)
        .then(function (response) {
            self.setState({
                task    : response.data,
                loading : false
            })
        })
        .catch(function (error) {
            console.log(error);
            self.setState({
                isi    : false,
                loading : false
            })
        });
    }

  render() {
      let icon = ''
      let status = ''
      let st_icon = ''
      let upload = ''
    
      let person = Object.assign({}, this.state.person);   

      if(cookie.load('user_id') == person.id){
          upload =
            <form action="assets/demo" data-toggle="dropzone" >
                <label class="btn btn-success">
                    Ubah Foto Profil
                    <input onChange={this.fileSelectedHandler} class="form-control" id="l8" onChange={this.fileSelectedHandler} type="file" style={{'display' : 'none'}}  />
                </label>
            </form>
      }

    return (

        <main class="main-wrapper clearfix">
        <div class="widget-list row">
                <div class="widget-holder widget-full-height col-md-12">
                    <div class="widget-bg">
                        <div class="widget-heading widget-heading-border">
                            <h5 class="widget-title">Profil Pegawai BPKAD</h5>
                            
                        </div>
                        
                        <div class="widget-body clearfix">
                            
                        <div class="col-12 col-md-12 mr-b-30">
                        <ul class="nav nav-tabs contact-details-tab">
                            <li class="nav-item"><a href="#profile-tab-bordered-1" class="nav-link active" data-toggle="tab">Informasi</a>
                            </li>
                            <li onClick={this.handleRiwayat} class="nav-item"><a href="#activity-tab-bordered-1" class="nav-link" data-toggle="tab">Riwayat Tugas</a>
                            </li>
                        </ul>
                        <div class="tab-content">

                            <div role="tabpanel" class="tab-pane active" id="profile-tab-bordered-1">
 
                                {
                                    this.state.loading ?
                                    <div style={{'marginLeft' : '45%'}}>
                                        <ClipLoader
                                            color={'#9013FE'} 
                                            loading={this.state.loading}
                                        />
                                    </div>
                                    :
                                    <div class="contact-details-profile">
                                        <div class="contact-info">
                                            <header>
                                                <figure class="inline-block thumb-md">
                                                    {
                                                        this.state.loadImage ? 
                                                        <BounceLoader
                                                            color={'#9013FE'} 
                                                            loading={this.state.loadImage}
                                                        />
                                                        :
                                                        <img src={this.state.person.foto} class="rounded-circle" alt="" />
                                                    }
                                                </figure>
                                                <h5 class="fw-500 mr-b-5"><a href="./page-profile.html">{this.state.person.nama}</a></h5><small class="text-muted">{this.state.person.jabatan}</small>
                                            </header>
                                            
                                            {upload}
                                            
                                        </div>

                                        <h5 class="mr-b-20">Profil Personal</h5>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="contact-details-cell"><small class="heading-font-family fw-500">Nama</small>  <span>{this.state.person.nama}</span>
                                                </div>
                                                
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="contact-details-cell"><small class="heading-font-family fw-500">Jabatan</small>  <span>{this.state.person.jabatan}</span>
                                                </div>
                                                
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="contact-details-cell"><small class="heading-font-family fw-500">NIP</small>  <span>{this.state.person.nip}</span>
                                                </div>
                                                
                                            </div>
                                            
                                        </div>
                                        
                                        <hr />
                                        
                                    </div>
                                }
                                
                            </div>

                            <div role="tabpanel" class="tab-pane" id="activity-tab-bordered-1">
                                
                                {
                                    this.state.loading ?
                                    <div style={{'marginLeft' : '45%'}}>
                                        <ClipLoader
                                            color={'#9013FE'} 
                                            loading={this.state.loading}
                                        />
                                    </div>
                                    :
                                    <div class="widget-list">
                                    <div class="row">
                                        <div class="widget-holder col-md-12">
                                            <div class="widget-bg-transparent">
                                                <div class="widget-body">
                                                    
                                                    {
                                                        this.state.isi ?
                                                        <div class="timeline">
                                                            
                                                            {
                                                                this.state.task.map(function(tugas){
                                                                    
                                                                    if(tugas.st_judul == 'Belum dikerjakan') {
                                                                        status  = 'bg-danger'
                                                                        st_icon = 'material-icons bg-danger'
                                                                        icon    = 'stop'
                                                                    }else if(tugas.st_judul == 'Sedang dikerjakan'){
                                                                        status  = 'bg-warning'
                                                                        st_icon = 'material-icons bg-warning'
                                                                        icon    = 'play_arrow'
                                                                    }else{
                                                                        status  = 'bg-success'
                                                                        st_icon = 'material-icons bg-success'
                                                                        icon    = 'done'
                                                                    }
                                                                    
                                                                    return(
                                                                        <div class="timeline-single">
                                                                            <div class="timeline-header"><i class={st_icon}>{icon}</i>
                                                                            </div>
                                                                            <div class="timeline-title">
                                                                                <img src={tugas.foto} alt="User Image" />
                                                                                <h5>{tugas.nama}</h5><small>Just now</small>
                                                                            </div>
                                                                            <div class="timeline-body">
                                                                                <header class={status}>
                                                                                    <h6 class="timeline-body-title">Judul Tugas : <a href="#">{tugas.judul}</a></h6>
                                                                                </header>
                                                                                <p>Detail tugas : {tugas.lv1} , {tugas.lv2} , {tugas.lv3}</p>
                                                                            </div>
                                                                        </div>
                                                                    )
                                                                })
                                                            }
                                                            
                                                            {/* <button class="load-more-btn btn btn-primary" data-url="assets/js/getTimeline.php"><span>Lebih Banyak</span>
                                                            </button> */}
                                                        </div>
                                                        :
                                                        <h1>Maaf, belum ada riwayat tugas ..</h1>
                                                    }

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }
                                
                            </div>
                            
                        </div>
                        
                    </div>
                            
                        </div>
                        
                    </div>
                    
                </div>
                
                
                
            </div>
            
      </main>

    );
  }
}

export default Detail;
