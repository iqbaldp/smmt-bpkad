import React, { Component } from 'react'
import cookie from 'react-cookies'
import { BeatLoader } from 'react-spinners'
import axios from 'axios'
import '../animate.css';
import {Animated} from "react-animated-css";

class App extends Component {

    constructor(props){
        super(props)
        this.state = {
            level1      : false,
            dLevel1     : false,
            dLevel2     : false,
            dLevel3     : false,
            judul       : '',
            lv1         : '',
            d_lv1       : '',
            st_d_lv1    : '',
            lv2         : '',
            d_lv2       : '',
            st_d_lv2    : '',
            lv3         : '',
            d_lv3       : '',
            st_d_lv3    : '',
            success     : false,
            text        : true,
            loading     : false,
            penerima    : '',
            user        : [],
            empty       : false,
            animatelv1  : false
        }
    }

    handleListLevel1 = () => {this.setState({level1 : !this.state.level1, animatelv1 : !this.state.animatelv1})}
    handleDListLevel1 = () => {this.setState({dLevel1 : !this.state.dLevel1})}
    handleDListLevel2 = () => {this.setState({dLevel2 : !this.state.dLevel2})}
    handleDListLevel3 = () => {this.setState({dLevel3 : !this.state.dLevel3})}
    // fill nilai state
    handleJudul = (event) => { this.setState({judul : event.target.value}) }
    handleLv1   = (event) => { this.setState({lv1   : event.target.value}) }
    handleLv2   = (event) => { this.setState({lv2   : event.target.value}) }
    handleLv3   = (event) => { this.setState({lv3   : event.target.value}) }
    handleDLv1  = (event) => { this.setState({d_lv1 : event.target.value}) }
    handleDLv2  = (event) => { this.setState({d_lv2 : event.target.value}) }
    handleDLv3  = (event) => { this.setState({d_lv3 : event.target.value}) }
    nama = (event) => { this.setState({penerima : event.target.value}) }

    submitTask = () => {
        const self = this
        
        self.setState({
            text : false,
            loading : true
        })

        if(self.state.judul !== '' && self.state.penerima !== ''){

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + cookie.load('token')
            axios.post('http://localhost:8000/api/task/', {
                nip_tujuan  : self.state.penerima,
                user_id     : cookie.load('user_id'),
                judul       : self.state.judul,
                lv1         : self.state.lv1,
                d_lv1       : self.state.d_lv1,
                lv2         : self.state.lv2,
                d_lv2       : self.state.d_lv2,
                lv3         : self.state.lv3,
                d_lv3       : self.state.d_lv3
                })
                .then(function (response) {

                setTimeout(() => {
                    self.setState({
                        text    : true,
                        loading : false,
                        success : true,
                        empty   : false
                    })
                }, 3500)

                })
                .catch(function (error) {
                console.log(error);
            });

        }else {
            self.setState({
                empty   : true,
                text    : true,
                loading : false
            })
        }
    }

    componentDidMount(){
        const self = this
        axios.get('http://localhost:8000/api/users')
        .then(function (response) {
            console.log(response.data)
            self.setState({user : response.data})
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    handleHidden(){
        this.setState({
            show : !this.state.show
        })
    }

  render() {
    let {level1} = this.state
    
    const dListLevel1 = (
        <ul>
            <li class="list-group-item d-flex" style={{'padding' : 10}}>
                <div class="mr-auto" style={{ 'width' : '100%'}}>
                    <input onChange={this.handleDLv1} style={{ 'border' : 'none'}} type="text" class="form-control form-control-md" placeholder="Detail Tugas" />
                </div>
                <div>
                </div>    
            </li>
        </ul>
    )

    const dListLevel2 = (
        <ul>
            <li class="list-group-item d-flex" style={{'padding' : 10}}>
                <div class="mr-auto" style={{ 'width' : '100%'}}>
                    <input onChange={this.handleDLv2} style={{ 'border' : 'none'}} type="text" class="form-control form-control-md" placeholder="Detail Tugas" />
                </div>
                <div>
                </div>    
            </li>
        </ul>
    )

    const dListLevel3 = (
        <ul>
            <li class="list-group-item d-flex" style={{'padding' : 10}}>
                <div class="mr-auto" style={{ 'width' : '100%'}}>
                    <input onChange={this.handleDLv3} style={{ 'border' : 'none'}} type="text" class="form-control form-control-md" placeholder="Detail Tugas" />
                </div>
                <div>
                </div>    
            </li>
        </ul>
    )

    return (
      
        <main class="main-wrapper clearfix">

            <div class="row page-title clearfix">
                <div class="page-title-left">
                    <h6 class="page-title-heading mr-0 mr-r-5">Buat Tugas Baru</h6>
                    <p class="page-title-description mr-0 d-none d-md-inline-block">Buat Rincian Tugas</p>
                </div>
            </div>

            <div class="widget-list row">
                <div class="widget-holder widget-full-height col-md-12">
                    <div class="widget-bg">
                        
                        <div class="widget-body">
                        <div class="widget-body clearfix">
                                <h5 class="box-title">Rincian Tugas <small>( Judul & Rincian Tugas )</small></h5>
                                <ul class="list-group sortable">
                                    <li class="list-group-item d-flex" style={{'padding' : 10}}>
                                        <div class="mr-auto" style={{ 'width' : '100%'}}>
                                            <input onChange={this.handleJudul} style={{ 'border' : 'none'}} type="text" class="form-control form-control-md" placeholder="Judul Tugas" />
                                        </div>
                                        <div><a href="#" onClick={this.handleListLevel1}><i class="material-icons list-icon">add</i></a>
                                        </div>    
                                    </li>
                                    
                                    <li class={ level1 ? null : "invisible" }>
                                        <ul>
                                        <Animated animationIn="bounceInRight" animationOut="pulse" isVisible={this.state.animatelv1}>
                                            <li class="list-group-item d-flex" style={{'padding' : 10}}>
                                                <div class="mr-auto" style={{ 'width' : '100%'}}>
                                                    <input onChange={this.handleLv1} style={{ 'border' : 'none'}} type="text" class="form-control form-control-md" placeholder="Detail Tugas" />
                                                </div>
                                                <div><a href="#" onClick={this.handleDListLevel1}><i class="material-icons list-icon">add</i></a>
                                                </div>    
                                            </li>
                                            </Animated>

                                                {this.state.dLevel1 ? dListLevel1 : null}
                                            
                                            <Animated animationIn="bounceInUp" animationOut="pulse" isVisible={this.state.animatelv1}>
                                            <li class="list-group-item d-flex" style={{'padding' : 10}}>
                                                <div class="mr-auto" style={{ 'width' : '100%'}}>
                                                    <input onChange={this.handleLv2} style={{ 'border' : 'none'}} type="text" class="form-control form-control-md" placeholder="Detail Tugas" />
                                                </div>
                                                <div><a href="#" onClick={this.handleDListLevel2}><i class="material-icons list-icon">add</i></a>
                                                </div>    
                                            </li>
                                            </Animated>

                                            {this.state.dLevel2 ? dListLevel2 : null}

                                            <Animated animationIn="bounceInLeft" animationOut="pulse" isVisible={this.state.animatelv1}>
                                            <li class="list-group-item d-flex" style={{'padding' : 10}}>
                                                <div class="mr-auto" style={{ 'width' : '100%'}}>
                                                    <input onChange={this.handleLv3} style={{ 'border' : 'none'}} type="text" class="form-control form-control-md" placeholder="Detail Tugas" />
                                                </div>
                                                <div><a href="#" onClick={this.handleDListLevel3}><i class="material-icons list-icon">add</i></a>
                                                </div> 
                                            </li>
                                            </Animated>

                                                {this.state.dLevel3 ? dListLevel3 : null}

                                        </ul>
                                    </li>

                                    <hr/>
                                    <h5 class="box-title">Penerima Tugas <small>( Nama & Jabatan )</small></h5>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <select class="form-control" id="l13" onChange={this.nama}>
                                                <option>Penerima Tugas</option> 
                                                {
                                                    this.state.user.filter(tn => tn.level <= cookie.load('level') && tn.id != cookie.load('user_id')).map(function (user){
                                                    return (
                                                        <option value={user.id}>
                                                            {user.nama} ( {user.jabatan} )
                                                        </option>
                                                    )
                                                    })
                                                }                
                                            </select>
                                        </div>
                                    </div>
                                    
                                    {
                                        this.state.success ? 
                                        <Animated animationIn="rubberBand" animationOut="pulse" isVisible={this.state.success}>
                                        <div class="alert alert-icon alert-success border-success alert-dismissible fade show" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                            </button> <i class="material-icons list-icon">check_circle</i>  <strong>Sukses !</strong> Tugas berhasil disimpan dan dikirimkan pada penerima.</div> 
                                        </Animated> : null
                                        
                                    }
                                    {
                                        this.state.empty ?
                                        <Animated animationIn="rubberBand" animationOut="pulse" isVisible={this.state.empty}>
                                        <div class="alert alert-icon alert-danger border-danger alert-dismissible fade show" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                            </button> <i class="material-icons list-icon">check_circle</i>  <strong>Gagal ! </strong>Mohon Maaf, judul tugas & penerima tugas harus diisi</div>
                                        </Animated>
                                        : null
                                    }

                                    <div class="form-actions">
                                        <div class="form-group row">
                                            <div class="col-md-12 ml-md-auto btn-list">
                                                <button onClick={this.submitTask} class="btn btn-primary btn-rounded" type="submit">{this.state.text ? "Simpan" : "Menyimpan tugas ..."}</button>
                                                <BeatLoader
                                                    color={'#408FE7'} 
                                                    loading={this.state.loading} 
                                                />
                                            </div>
                                        </div>
                                    </div>

                                </ul>                     
                            </div>
                            
                        </div>
                        
                    </div>
                    
                </div>
                
                
                
            </div>
            
        </main>

    );
  }
}

export default App;
