import React, { Component } from 'react';
import '../animate.css';
import {Animated} from "react-animated-css";
import axios from 'axios'
import { NavLink } from "react-router-dom";
import cookie from 'react-cookies';

class TaskSent extends Component {

    constructor(props){
        super(props)

        this.state = {
            detail : [],
            id     : null,
            judul  : '',
            success : false
        }
    }

    handlegetData = (id, judul) => {
        this.setState({
            id, judul, success : false
        })
    }

    handleDeleteTask = () => {
        const self = this
        axios.delete('http://localhost:8000/api/taskuser/' + self.state.id + '/' + cookie.load('user_id'))
        .then(function (response) {
            self.setState({detail : response.data, success : true})
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    componentDidMount(){
        const self = this

        //load task
        axios.get('http://localhost:8000/api/tasksent/' + cookie.load('user_id'))
        .then(function (response) {
            console.log("Task " + response.data)
            self.setState({detail : response.data})
        })
        .catch(function (error) {
            console.log(error);
        });
  
    }

  render() {
    let number  = 0
    let status  = ''
    let presentase  = 0

    const self = this

    return (

        <main class="main-wrapper clearfix">

        <div class="row page-title clearfix">
            <div class="page-title-left">
                <h6 class="page-title-heading mr-0 mr-r-5">Daftar Tugas Keluar</h6>
                <p class="page-title-description mr-0 d-none d-md-inline-block">Rincian & Status Tugas</p>
            </div>
        </div>

        <br/>
        {
            this.state.success ?
            <Animated animationIn="rubberBand" animationOut="pulse" isVisible={this.state.success}>
            <div class="alert alert-icon alert-success border-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button> <i class="material-icons list-icon">check_circle</i>  <strong>Sukses!</strong> Tugas berhasil dihapus.</div>
            </Animated>
            : null
        }

        <div class="widget-list row">
                <div class="widget-holder widget-full-height col-md-12">
                    <div class="widget-bg">
                        
                        <div class="widget-body">
                            
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Judul</th>
                                            <th>Progress</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.detail.map(function(detail){
                                                number++

                                                if(detail.st_judul == "Belum dikerjakan") status = "progress-bar bg-danger"
                                                else if(detail.st_judul == "Sedang dikerjakan") status = "progress-bar bg-warning"
                                                else status = "progress-bar bg-success"

                                                if(detail.st_judul == "Belum dikerjakan") { presentase = 0 }
                                                else if(detail.st_judul == "Sedang dikerjakan") {
                                                    if(detail.st_lv1 == "Selesai" &&
                                                    detail.st_lv2 == "Selesai" || detail.st_lv2 == "Sedang dikerjakan" &&
                                                    detail.st_lv2 == "Selesai" || detail.st_lv2 == "Sedang dikerjakan"){
                                                        presentase = 70
                                                    }
                                                    else if(detail.st_lv1 == "Selesai" || detail.st_lv1 == "Sedang dikerjakan" &&
                                                    detail.st_lv2 == "Selesai" || detail.st_lv2 == "Sedang dikerjakan" &&
                                                    detail.st_lv2 == "Selesai" || detail.st_lv2 == "Sedang dikerjakan"){
                                                        presentase = 50
                                                    }
                                                    else if(detail.st_lv1 == "Selesai" || detail.st_lv1 == "Sedang dikerjakan" &&
                                                    detail.st_lv2 == "Belum dikerjakan" || detail.st_lv2 == "Sedang dikerjakan" &&
                                                    detail.st_lv2 == "Belum dikerjakan" || detail.st_lv2 == "Sedang dikerjakan"){
                                                        presentase = 30
                                                    }
                                                }
                                                else presentase = 100

                                                return(
                                                    <tr>
                                                        <th>{number}</th>
                                                        <td>
                                                        <NavLink  to={"/detail/" + detail.id} >
                                                            {detail.judul}
                                                        </NavLink>
                                                        </td>
                                                        <td>
                                                            <div class="progress" data-toggle="tooltip" title="80%">
                                                                <div class={status} role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style={{'width' : presentase + '%'}}><span class="sr-only">80% Complete</span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                        <b onClick={() => self.handlegetData(detail.id, detail.judul)} data-toggle="modal" data-target=".bs-modal-md"  style={{'cursor' : 'pointer', 'textAlign' : 'center'}} data-toggle="modal" data-target=".bs-modal-md" class="text-danger">Hapus</b>
                                                         
                                                        </td>
                                                    </tr>
                                                )
                                            })
                                        }
                                    </tbody>
                                </table>
                                
                                <div class="modal modal-danger fade bs-modal-md" tabindex="-1" role="dialog" aria-labelledby="myMediumModalLabel" aria-hidden="true" style={{"display": "none"}}>
                                <div class="modal-dialog modal-md">
                                    <div class="modal-content">
                                        <div class="modal-header text-inverse">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h5 class="modal-title" id="myMediumModalLabel">Hapus Pengguna</h5>
                                        </div>
                                        <div class="modal-body">
                                            <h5>Apakah anda yakin ingin menghapus tugas ini ?</h5>
                                            <p> Judul  : {this.state.judul}</p>
                                        </div>
                                        <div class="modal-footer"><a href="#" data-dismiss="modal" class="btn btn-info btn-rounded ripple text-left">Batal</a> 
                                            <button type="button" class="btn btn-danger btn-rounded ripple text-left" data-dismiss="modal" onClick={this.handleDeleteTask}>Hapus</button>
                                        </div>
                                    </div>
                                    
                                </div>
                                </div>

                        </div>
                        
                    </div>
                    
                </div>
                
                
                
            </div>
            
      </main>

    );
  }
}

export default TaskSent;
