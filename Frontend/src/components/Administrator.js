import React, { Component } from 'react';
import axios from 'axios'
import cookie from 'react-cookies';

class Administrator extends Component {

    constructor(props){
        super(props)
        this.state = {
            admin    : [],
            username : '',
            password : '',
            visibility : false
        }
        
    }

    componentDidMount(){
        const self = this
        axios.get('http://localhost:8000/api/admin')
        .then(function (response) {
            console.log("response.data " + response)
            self.setState({admin : response.data})
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    handleUsername = (e) => { this.setState({username : e.target.value}) }
    handlePassword = (e) => { this.setState({password : e.target.value}) }

    openForm = () => {
        this.setState({
            visibility : !this.state.visibility
        })
    }

    handleChangeAdmin = () => {
        const self = this
        axios.put('http://localhost:8000/api/admin/', {
           username : self.state.username,
           password : self.state.password
        })
        .then(function (response) {
            self.setState({
                visibility : false,
                admin : response.data
            })
        })
        .catch(function (error) {
            console.log(error);
        });

    }

  render() {
    return (

        <main class="main-wrapper clearfix">

            <div class="widget-list row">
                <div class="widget-holder widget-full-height col-md-12">
                    <div class="widget-bg">
                        <div class="widget-heading widget-heading-border">
                            <div class="widget-heading clearfix">
                                <h5>Kelola Akun SMMT BPKAD</h5>
                            </div>
                        </div>
                        
                        <div class="widget-body">
                            
                        <div class="widget-list">
                            <div class="row">
                                <div class="col-md-12 widget-holder">
                                    <div class="widget-bg">
                                        <div class="widget-body clearfix">
                                            <table class="table table-editable table-responsive">
                                                <thead>
                                                    <tr>
                                                        <th>Username {this.state.admin.username}</th>
                                                        <th>Password</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                {
                                                    this.state.admin.map((adm) => {
                                                        return(
                                                            <tr>
                                                                <td>{adm.username}</td>
                                                                <td>{adm.password}</td>
                                                                <td><b onClick={this.openForm} style={{'cursor' : 'pointer'}} class="text-warning">Sunting</b></td>
                                                            </tr>
                                                        )
                                                    })
                                                }
                                                </tbody>

                                            </table>
                                            {
                                                this.state.visibility ? 
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <input onChange={this.handleUsername} class="form-control" value={this.state.username} type="text" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <input onChange={this.handlePassword} class="form-control" value={this.state.password} type="text" />
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-actions btn-list">
                                                        <button onClick={this.handleChangeAdmin} class="btn btn-primary" type="button">Ubah</button>
                                                    </div>

                                                </div>
                                                :null
                                            }
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                                
                            </div>
                            
                        </div>
                            
                        </div>
                        
                    </div>
                    
                </div>
                
                
                
            </div>
            
      </main>

    );
  }
}

export default Administrator;
