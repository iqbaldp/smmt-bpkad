import React, { Component } from 'react';
import cookie from 'react-cookies';
import axios from 'axios'
import { NavLink, Link } from "react-router-dom";

class Header extends Component {

    constructor(props){

        super(props)

        this.state = {
            user    : [],
            notif   : []
        }

    }


    componentDidMount(){
        const self = this
        //Get Data Profile
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + cookie.load('token')

        axios.get('http://localhost:8000/api/profile')
        .then(function (response) {
            self.setState({user : response.data})
        })
        .catch(function (error) {
            console.log(error);
        });

    }

    handleLogOut = () => {
        cookie.remove('token')
        cookie.remove('user_id')
        cookie.remove('access')
        window.location = "/";
    }

  render() {
    return (
      
        <nav class="navbar">
        
            <div class="navbar-header">
                <a href="#" class="navbar-brand">
                    <img class="logo-expand" alt="" src="./assets/img/logo2.png" width="100" />
                    <img class="logo-collapse" alt="" src="./assets/img/logo2.png" width="100" />
                   
                </a>
            </div>

            <ul class="nav navbar-nav">
                <li class="sidebar-toggle"><a href="javascript:void(0)" class="ripple"><i class="feather feather-menu list-icon fs-20"></i></a>
                </li>
            </ul>
        
            <div class="spacer"></div>
            
            <div class="btn-list dropdown d-none d-md-flex mr-4">
            <NavLink  to={"/create"} ><a href="javascript:void(0);" class="btn btn-primary" ><i class="feather feather-plus list-icon"></i> Tugas Baru</a></NavLink>
            </div>
            
            
            <ul class="nav navbar-nav d-none d-lg-flex ml-2">
                
            </ul>

            <ul class="nav navbar-nav">
                <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle ripple" data-toggle="dropdown"><span class="avatar thumb-xs2"><img src={this.state.user['foto']} class="rounded-circle" alt="" /> <i class="feather feather-chevron-down list-icon"></i></span></a>
                    <div class="dropdown-menu dropdown-left dropdown-card dropdown-card-profile animated flipInY">
                        <div class="card">
                            <ul class="list-unstyled card-body">
                                <NavLink to={"/profile"} >
                                    <li><a href="#"><span><span class="align-middle">Profil</span></span></a>
                                    </li>
                                </NavLink>
                                <li onClick={this.handleLogOut}><a href="#"><span><span class="align-middle">Keluar</span></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>

    </nav>

    );
  }
}

export default Header;
