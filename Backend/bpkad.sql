-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 09 Apr 2018 pada 06.06
-- Versi server: 10.1.30-MariaDB
-- Versi PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bpkad`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_task` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_tujuan` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto_nip` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `komentar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto_komentar` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'unread',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `comments`
--

INSERT INTO `comments` (`id`, `id_task`, `user_id`, `user_tujuan`, `foto_nip`, `komentar`, `foto_komentar`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '2', '1', 'https://i.imgur.com/zSH5OjF.jpg', 'testing komentar', NULL, 'unread', '2018-03-18 15:49:10', '2018-03-18 15:49:10'),
(2, 1, '3', '#12#', 'https://i.imgur.com/zSH5OjF.jpg', 'kgjhghhg', NULL, 'unread', '2018-03-18 18:23:15', '2018-03-18 18:23:15'),
(3, 9, '3', '1', 'https://i.imgur.com/z7558zD.jpg', 'ada', NULL, 'unread', '2018-03-26 18:14:42', '2018-03-26 18:14:42'),
(13, 1, '3', '2', 'https://i.imgur.com/thBNNXf.jpg', 'halow', NULL, 'unread', '2018-04-01 18:14:27', '2018-04-01 18:14:27'),
(14, 10, '2', '1', 'https://i.imgur.com/z7558zD.jpg', 'dfdf', NULL, 'unread', '2018-04-08 18:42:37', '2018-04-08 18:42:37'),
(15, 8, '2', '6', 'https://i.imgur.com/zSH5OjF.jpg', 'ghgshgsaa', NULL, 'unread', '2018-04-08 18:43:31', '2018-04-08 18:43:31');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2018_03_01_140331_create_tasks_table', 1),
(3, '2018_03_01_141623_create_comments_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `nip_tujuan` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `judul` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `st_judul` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lv1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `st_lv1` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `d_lv1` text COLLATE utf8mb4_unicode_ci,
  `st_d_lv1` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lv2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `st_lv2` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `d_lv2` text COLLATE utf8mb4_unicode_ci,
  `st_d_lv2` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lv3` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `st_lv3` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `d_lv3` text COLLATE utf8mb4_unicode_ci,
  `st_d_lv3` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tasks`
--

INSERT INTO `tasks` (`id`, `user_id`, `nip_tujuan`, `judul`, `st_judul`, `lv1`, `st_lv1`, `d_lv1`, `st_d_lv1`, `lv2`, `st_lv2`, `d_lv2`, `st_d_lv2`, `lv3`, `st_lv3`, `d_lv3`, `st_d_lv3`, `created_at`, `updated_at`) VALUES
(1, 3, '2', 'Perancangan jaringan LAN serta VLAN di lantai 2 dengan metode VLAN', 'Sedang dikerjakan', 'Pembelian alat dan bahan', 'Selesai', NULL, 'Belum dikerjakan', 'Pembuatan struktur LAN', 'Belum dikerjakan', NULL, 'Belum dikerjakan', 'Konfigurasi VLAN', 'Belum dikerjakan', NULL, 'Belum dikerjakan', '2018-04-01 15:56:30', '2018-04-01 15:56:30'),
(2, 1, '2', 'Perancangan database NoSQL', 'Sedang dikerjakan', 'Menggunakan Firebase', 'Sedang dikerjakan', NULL, 'Belum dikerjakan', 'Menggunakan MongoDB', 'Belum dikerjakan', NULL, 'Belum dikerjakan', 'Menggunakan AWS', 'Belum dikerjakan', NULL, 'Belum dikerjakan', '2018-04-01 20:26:27', '2018-04-01 20:26:27'),
(3, 1, '2', 'Perancangan Algoritma Computer Vision', 'Belum dikerjakan', 'Cari dataset', 'Belum dikerjakan', NULL, 'Belum dikerjakan', 'Pengumpulan feature', 'Belum dikerjakan', NULL, 'Belum dikerjakan', 'hahahaha', 'Belum dikerjakan', NULL, 'Belum dikerjakan', '2018-04-01 20:31:40', '2018-04-01 20:31:40'),
(4, 1, '2', 'Pembuatan Laporan Keuangan', 'Belum dikerjakan', 'Pembuatan Laporan Keuangan', 'Belum dikerjakan', NULL, 'Belum dikerjakan', 'Pembuatan Laporan Keuangan', 'Belum dikerjakan', NULL, 'Belum dikerjakan', 'hahahaha', 'Belum dikerjakan', NULL, 'Belum dikerjakan', '2018-04-01 20:32:09', '2018-04-01 20:32:09'),
(6, 1, '2', 'Ini judul nyaa', 'Sedang dikerjakan', 'wkwkwkwk', 'Selesai', NULL, 'Belum dikerjakan', 'wkwkwkw', 'Sedang dikerjakan', NULL, 'Belum dikerjakan', 'ajsnasnajsasjkas', 'Belum dikerjakan', NULL, 'Belum dikerjakan', '2018-04-01 20:53:12', '2018-04-01 20:53:12'),
(7, 3, '1', 'Selesaikan laporanmuu', 'Belum dikerjakan', 'BAB 1 aja duluuuu', 'Belum dikerjakan', NULL, 'Belum dikerjakan', 'lanjut ngoding ntarr', 'Belum dikerjakan', NULL, 'Belum dikerjakan', 'inget spring sama iris', 'Belum dikerjakan', NULL, 'Belum dikerjakan', '2018-04-01 20:54:46', '2018-04-01 20:54:46'),
(8, 2, '6', 'Pembuatan Machine Learning', 'Selesai', 'Pengumpulan Dataset', 'Belum dikerjakan', NULL, 'Belum dikerjakan', 'Penentuan Algoritma', 'Selesai', NULL, 'Belum dikerjakan', 'Implementasi', 'Selesai', NULL, 'Belum dikerjakan', '2018-04-08 22:52:04', '2018-04-07 22:52:04'),
(9, 6, '2', 'Pembuatan Sistem Deteksi Penipuan', 'Belum dikerjakan', 'Rancang Data', 'Belum dikerjakan', NULL, 'Belum dikerjakan', 'Cari Algoritma', 'Belum dikerjakan', NULL, 'Belum dikerjakan', 'Gunakan Deep Learning', 'Belum dikerjakan', NULL, 'Belum dikerjakan', '2018-04-07 23:38:25', '2018-04-07 23:38:25'),
(10, 1, '2', 'aaaaa', 'Belum dikerjakan', 'bb', 'Belum dikerjakan', NULL, 'Belum dikerjakan', 'bbbbbb', 'Belum dikerjakan', NULL, 'Belum dikerjakan', 'bbb', 'Belum dikerjakan', NULL, 'Belum dikerjakan', '2018-04-08 18:41:49', '2018-04-08 18:41:49');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nip` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `nip`, `nama`, `jabatan`, `password`, `foto`, `created_at`, `updated_at`) VALUES
(1, '123456', 'Muhammad Iqbal Dwi Putra', 'Sekretaris', '$2y$10$uyA93TxvGrS1DQxk45rl0udqSu0J98TPNsWFWcibY5RKT1dFeJSGC', 'https://i.imgur.com/z7558zD.jpg', '2018-03-18 03:36:35', '2018-03-18 03:36:35'),
(2, '654321', 'Aldian Wahyu Septiadi', 'Ketua Umum', '$2y$10$YBd0Z5dIg/agtTCMpJz5U.xpi1IpgHgUpKUIBOFmb8ywT0Y3wcK1m', 'https://i.imgur.com/zSH5OjF.jpg', '2018-03-18 03:47:27', '2018-03-18 03:47:27'),
(3, '111', 'aaaaa', 'Ketua', '$2y$10$G7hvXVFKYlfc18aZEZ2lWe9hYAT9pS0p/BlxjVJDjT8kaKTPrFfDy', 'https://i.imgur.com/thBNNXf.jpg', '2018-03-18 18:20:30', '2018-03-18 18:20:30'),
(4, '999', 'Arief  Rahman Hakim', 'Bendahara Umum', '$2y$10$Gn89i.HzZi6vDaeqsztg4eZ279iASn4s4Y9tnm69boa9tbPJd0pqu', 'https://i.imgur.com/kYWNvGe.png', '2018-04-07 02:33:11', '2018-04-07 02:33:11'),
(6, '222', 'Ahmad Fatoni Dwi', 'Kepala Bidang Akuntansi dan Pelaporan', '$2y$10$pAvqwGSBvT/bhimcfXxM9u.1KldnmJRJ0OFNesXZ1mdCBsoGMLNH.', 'https://i.imgur.com/zCI06kD.png', '2018-04-07 22:34:05', '2018-04-07 22:34:05');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_task` (`id_task`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_nip_unique` (`nip`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `id_task` FOREIGN KEY (`id_task`) REFERENCES `tasks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
