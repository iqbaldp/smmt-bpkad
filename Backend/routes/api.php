<?php

Route::group(['middleware' => ['api']], function(){

	Route::post('auth/signup', 'AuthController@signup');
	Route::post('auth/signupimage', 'AuthController@upload');
	Route::post('auth/signin', 'AuthController@signin');

	Route::get('/task/{limit}', 'TaskController@index');
	Route::get('/task', 'TaskController@all');
	Route::delete('/task/{id}', 'TaskController@destroy');
	Route::delete('/taskuser/{id}/{iduser}', 'TaskController@destroyTask');
	Route::get('/task/{id}', 'TaskController@show');
	Route::put('/task/{column}/{id}', 'TaskController@update');
	Route::get('/tasksent/{id}', 'TaskController@showSent');
	Route::get('/taskreceive/{id}', 'TaskController@showReceive');
	//Route::post
	Route::get('/detail/pemberi/{id}', 'TaskController@pemberiTask');
	Route::get('/detail/penerima/{id}', 'TaskController@penerimaTask');
	Route::get('/users', 'UserController@index');
	Route::delete('/users/{id}', 'UserController@destroy');
	Route::put('/users/{id}', 'UserController@update');
	Route::put('/users/image/{id}', 'UserController@updateImage');
	Route::get('/usersbylevel/{level}', 'UserController@usersbylevel');
	Route::get('/users/{id}', 'UserController@showUser');
	Route::post('/task', 'TaskController@store');
	Route::post('/comment', 'CommentController@store');
	Route::get('/comment', 'CommentController@index');
	Route::get('/comment/{idtask}/{limit}', 'CommentController@show');
	Route::get('/admin', 'AdminController@index');
	Route::post('/admin', 'AdminController@auth');
	Route::put('/admin', 'AdminController@update');
	
	Route::group(['middleware' => ['jwt.auth']], function(){


		Route::get('/profile', 'UserController@show');
		

	});

});