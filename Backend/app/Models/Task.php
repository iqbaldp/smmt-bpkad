<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    
	protected $fillable = [
		'judul',
		'st_judul',
		'lv1',
		'st_lv1',
		'd_lv1',
		'st_d_lv1',
		'lv2',
		'st_lv2',
		'd_lv2',
		'st_d_lv2',
		'lv3',
		'st_lv3',
		'd_lv3',
		'st_d_lv3',
		'nip_tujuan',
		'user_id',
    ];

    public function user(){

    	return $this->belongsTo('App\Models\User');

    }

}
