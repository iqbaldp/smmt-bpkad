<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;
use Illuminate\Support\Facades\DB;

class CommentController extends Controller
{

    public function index(Request $response)
    {
        return Comment::all();
    }

    public function show($idtask, $limit)
    {
        $data = DB::select( DB::raw("SELECT * FROM `comments` JOIN users  On user_id = users.id AND id_task = $idtask ORDER BY comments.created_at DESC LIMIT $limit") );
        return $data;
    }

    public function store(Request $request)
    {   
        $idtask = request()->json('id_task');
        $limit  = request()->json('limit');
        
    	$comment = Comment::create([

    		'id_task' 		=> request()->json('id_task'),
    		'user_id' 	    => request()->json('user_id'),
            'user_tujuan'   => request()->json('user_tujuan'),
    		'komentar'  	=> request()->json('komentar'),
    		'foto_komentar' => request()->json('foto_komentar'),
            'foto_nip'      => request()->json('foto_nip')
            

    	]);

    	$data = DB::select( DB::raw("SELECT * FROM `comments` JOIN users  On user_id = users.id AND id_task = $idtask ORDER BY comments.created_at DESC LIMIT $limit") );

        return $data;
    }	
}
