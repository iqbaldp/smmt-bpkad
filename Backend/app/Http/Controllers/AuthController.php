<?php

namespace App\Http\Controllers;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\User;

class AuthController extends Controller
{

    function upload(Request $request)
    {
        $file       = $request->file('image');
        $fileName   = $file->getClientOriginalName();
        $request->file('image')->move("images/", $fileName);

        return response()->json(['name' => $fileName], 200);
    }

    function signup(Request $request){

        $level = 0;
        $role  = 0;

        if ($request->json('jabatan') === "Staf") {
            $level = 0;
        }else if ($request->json('jabatan') === "Kasi Pemanfaatan Aset" || 
                $request->json('jabatan') === "Kasi Pengamanan Aset" || 
                $request->json('jabatan') === "Kasubbag Tata Usaha UPTD" ||
                $request->json('jabatan') === "Kasubbag Tata Usaha Pengelola Islamic Center" ||
                $request->json('jabatan') === "Kasi Pemeliharaan Sarana dan Prasarana" ||
                $request->json('jabatan') === "Kasi Pemanfaatan, Pengembangan Usaha dan Bisnis" ||
                $request->json('jabatan') === "Kasubbid Perencanaan Kebutuhan dan Pengadaan BMD" ||
                $request->json('jabatan') === "Kasubbid Pemeliharaan dan Penghapusan BMD" ||
                $request->json('jabatan') === "Kasubbid Penatausahaan dan Pembinaan Aset" ||
                $request->json('jabatan') === "Kasubbid Perbendaharaan II" ||
                $request->json('jabatan') === "Kasubbid Perbendaharaan I" ||
                $request->json('jabatan') === "Kasubbid Pengelolaan KAS" ||
                $request->json('jabatan') === "Kasubbid Pelaporan dan Evaluasi Keuangan Daerah" ||
                $request->json('jabatan') === "Kasubbid Akuntansi II" ||
                $request->json('jabatan') === "Kasubbid Akuntansi I" ||
                $request->json('jabatan') === "Kasubbid Bina dan Evaluasi Anggaran Kabupaten/Kota" ||
                $request->json('jabatan') === "Kasubbid Anggaran II" ||
                $request->json('jabatan') === "Kasubbid Anggaran I" ||
                $request->json('jabatan') === "Kasubbag Keuangan" ||
                $request->json('jabatan') === "Kasubbag Umum" ||
                $request->json('jabatan') === "Kasubbag Program" ) {
            $level = 1;
        }else if ($request->json('jabatan') === "Sekretaris" || 
                $request->json('jabatan') === "Kepala Bidang Anggaran" ||
                $request->json('jabatan') === "Kepala Bidang Akuntansi dan Pelaporan" ||
                $request->json('jabatan') === "Kepala Bidang Perbendaharaan" ||
                $request->json('jabatan') === "Kepala Bidang Pengelolaan BMD" ||
                $request->json('jabatan') === "Kepala Unit Pengelola Islamic Center" ||
                $request->json('jabatan') === "Kepala UPTD Balai Pemanfaatan dan Pengamanan Aset") {
            $level = 2;
        }else if ($request->json('jabatan') === "Kepala BPKAD" ) {
            $level = 3;
        }


    	User::create([

    		'nip' 		=> $request->json('nip'),
    		'nama'		=> $request->json('nama'),
    		'jabatan'	=> $request->json('jabatan'),
    		'foto'		=> 'http://'.$request->getHttpHost().'/images/'.$request->json('foto'),
            'role'      => $role,
            'level'     => $level,
    		'password'	=> bcrypt($request->json('password'))

    	]);

    }

    function signin(Request $request){

    	// grab credentials from the request
        $credentials = $request->only('nip', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json([

        	'user_id' 	=> $request->user()->id,
            'role'      => $request->user()->role,
            'level'     => $request->user()->level,
        	'token' 	=> $token

        ]);

    }

}
