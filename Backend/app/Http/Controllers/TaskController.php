<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Task;

class TaskController extends Controller
{

	public function index($limit)
	{

		$data = DB::table('users')
	            ->join('tasks', 'users.id', '=', 'tasks.user_id')
	            ->select('tasks.id', 'users.nama', 'users.foto', 'tasks.judul', 'tasks.st_judul', 'tasks.created_at')
	            ->orderby('tasks.created_at', 'desc')
	            ->limit($limit)
	            ->get();

	    return $data;

	}

	public function all()
	{
		$data = DB::select( DB::raw("SELECT *, tasks.id AS id_task FROM `tasks` JOIN users ON user_id = users.id ORDER BY tasks.created_at DESC") );

		return $data;
	}

	public function penerimaTask($id)
	{
		$data = DB::select( DB::raw("SELECT * FROM `tasks` JOIN users ON users.id = tasks.nip_tujuan WHERE tasks.id = $id") );

		return $data;
	}

	public function pemberiTask($id)
	{
		$data = DB::select( DB::raw("SELECT * FROM `tasks` JOIN users ON users.id = tasks.user_id WHERE tasks.id = $id") );

		return $data;
	}

	public function show($id)
	{

		$task = DB::select( DB::raw("SELECT * FROM `tasks` JOIN users ON users.id = tasks.user_id WHERE tasks.id = $id") );

		if (!$task) 
			return response()->json(['error' => 'id tugas tidak ditemukan'], 404);

		return $task;

	}

	public function update(Request $request, $column, $id){

		$data = $request->json('ubah');

		DB::table('tasks')
            ->where('id', $id)
            ->update([$column => $data]);
		return response()->json(['message' => 'success'], 200);
	}

	public function showSent($id)
	{

		$task = DB::select( DB::raw("SELECT * FROM `tasks` where user_id = $id
			ORDER BY tasks.created_at desc") );

		if (!$task) 
			return response()->json(['error' => 'id tugas tidak ditemukan'], 404);

		return $task;

	}

	public function showReceive($id)
	{

		$task = DB::select( DB::raw("SELECT *, tasks.id as id_task, tasks.created_at as dibuat FROM `tasks` JOIN users WHERE user_id = users.id AND nip_tujuan = $id ORDER BY tasks.created_at desc") );

		if (!$task) 
			return response()->json(['error' => 'id tugas tidak ditemukan'], 404);

		return $task;

	}

	public function destroy($id)
	{

		DB::select( DB::raw("DELETE FROM tasks WHERE id = ".$id) );

		$data = DB::select( DB::raw("SELECT *, tasks.id AS id_task FROM `tasks` JOIN users ON user_id = users.id ORDER BY tasks.created_at DESC") );

		return $data;

	}

	public function destroyTask($id, $id_user)
	{

		DB::select( DB::raw("DELETE FROM tasks WHERE id = ".$id) );

		$task = DB::select( DB::raw("SELECT * FROM `tasks` where user_id = $id_user
			ORDER BY tasks.created_at desc") );

		return $task;

	}

    public function store(Request $request)
    {

    	$task = Task::create([

    		'nip_tujuan' 	=> $request->json('nip_tujuan'),
    		'user_id' 		=> $request->json('user_id'),
    		'judul' 		=> $request->json('judul'),
    		'st_judul' 		=> "Belum dikerjakan",
    		'lv1' 			=> $request->json('lv1'),
    		'st_lv1' 		=> "Belum dikerjakan",
    		'd_lv1' 		=> $request->json('d_lv1'),
    		'st_d_lv1'	 	=> "Belum dikerjakan",
    		'lv2' 			=> $request->json('lv2'),
    		'st_lv2' 		=> "Belum dikerjakan",
    		'd_lv2' 		=> $request->json('d_lv2'),
    		'st_d_lv2' 		=> "Belum dikerjakan",
    		'lv3' 			=> $request->json('lv3'),
    		'st_lv3' 		=> "Belum dikerjakan",
    		'd_lv3' 		=> $request->json('d_lv3'),
    		'st_d_lv3' 		=> "Belum dikerjakan",

    	]);

    	return response()->json(['data' => $task], 200);

    }
}